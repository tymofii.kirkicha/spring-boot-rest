package com.messenger.message;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/messages")
public class MessageController {

    private MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping(value = "/all")
    public List<Message> getAllMessages() {
        return messageService.getAllMessages();
    }

    @GetMapping(value = "/{id}")
    public Message getMessageById(@RequestParam Integer id) {
        return messageService.getMessageById(id);
    }

    @PostMapping(value = "/create")
    public Message createMessage(@RequestParam String message,
                                 @RequestParam String author) {
        return messageService.createMessage(message, author);
    }

    @PutMapping(value = "/update/{@id}")
    public Message updateMessageById(@PathVariable Integer id,
                                     @RequestParam String message,
                                     @RequestParam String author) {
        return messageService.updateMessageById(id, message, author);
    }

    @DeleteMapping
    @ResponseStatus(code = HttpStatus.OK)
    public void deleteMessageById(@PathVariable Integer id) {
        messageService.deleteMessageById(id);
    }
}
