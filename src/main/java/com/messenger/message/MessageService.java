package com.messenger.message;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MessageService {

    private MessageRepository messageRepository;

    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public List<Message> getAllMessages() {
        return messageRepository.findAll();
    }

    public Message getMessageById(Integer id) {
        Optional<Message> optionalMessage = messageRepository.findById(id);
        return optionalMessage.orElse(null);
    }

    public Message createMessage(String message, String author) {
        Message newMessage = new Message(message, author);
        return messageRepository.save(newMessage);
    }

    public Message updateMessageById(Integer id, String message, String author) {
        Optional<Message> optionalMessage = messageRepository.findById(id);

        Message foundMessage;
        if (optionalMessage.isPresent()) {
            foundMessage = optionalMessage.get();
            foundMessage.setMessage(message);
            foundMessage.setAuthor(author);

            return messageRepository.save(foundMessage);
        } else {
            return null;
        }
    }

    public void deleteMessageById(Integer id) {
        Optional<Message> optionalMessage = messageRepository.findById(id);

        Message foundMessage;
        if (optionalMessage.isPresent()) {
            foundMessage = optionalMessage.get();
            messageRepository.delete(foundMessage);
        }
    }
}
